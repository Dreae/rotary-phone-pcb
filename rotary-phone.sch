EESchema Schematic File Version 4
LIBS:rotary-phone-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Rotary Phone"
Date "2018-08-30"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Transistor_BJT:TIP120 Q1
U 1 1 5B88A2D6
P 5800 3050
F 0 "Q1" H 6007 3096 50  0000 L CNN
F 1 "TIP120" H 6007 3005 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 6000 2975 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/TI/TIP120.pdf" H 5800 3050 50  0001 L CNN
	1    5800 3050
	1    0    0    -1  
$EndComp
$Comp
L Isolator:4N35 U2
U 1 1 5B88A4A8
P 5000 3050
F 0 "U2" H 5000 3375 50  0000 C CNN
F 1 "4N35" H 5000 3284 50  0000 C CNN
F 2 "Package_DIP:DIP-6_W7.62mm_SMDSocket_SmallPads" H 4800 2850 50  0001 L CIN
F 3 "https://www.vishay.com/docs/81181/4n35.pdf" H 5000 3050 50  0001 L CNN
F 4 "https://www.mouser.com/ProductDetail/Lite-On/4N35S?qs=sGAEpiMZZMu0f%252bT2bkVfun4Vzqn4U9UP" H 5000 3050 50  0001 C CNN "ProductLink"
	1    5000 3050
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:TIP120 Q2
U 1 1 5B88AA72
P 5800 3750
F 0 "Q2" H 6007 3796 50  0000 L CNN
F 1 "TIP120" H 6007 3705 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 6000 3675 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/TI/TIP120.pdf" H 5800 3750 50  0001 L CNN
	1    5800 3750
	1    0    0    -1  
$EndComp
$Comp
L Isolator:4N35 U3
U 1 1 5B88AA79
P 5000 3750
F 0 "U3" H 5000 4075 50  0000 C CNN
F 1 "4N35" H 5000 3984 50  0000 C CNN
F 2 "Package_DIP:DIP-6_W7.62mm_SMDSocket_SmallPads" H 4800 3550 50  0001 L CIN
F 3 "https://www.vishay.com/docs/81181/4n35.pdf" H 5000 3750 50  0001 L CNN
F 4 "https://www.mouser.com/ProductDetail/Lite-On/4N35S?qs=sGAEpiMZZMu0f%252bT2bkVfun4Vzqn4U9UP" H 5000 3750 50  0001 C CNN "ProductLink"
	1    5000 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 3150 5600 3150
Wire Wire Line
	5600 3150 5600 3050
Wire Wire Line
	5300 3850 5600 3850
Wire Wire Line
	5600 3850 5600 3750
$Comp
L Mechanical:MountingHole_Pad MH9
U 1 1 5B88C637
P 6600 3650
F 0 "MH9" V 6554 3800 50  0000 L CNN
F 1 "GND" V 6645 3800 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 6600 3650 50  0001 C CNN
F 3 "~" H 6600 3650 50  0001 C CNN
	1    6600 3650
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH8
U 1 1 5B88C6D9
P 6700 4000
F 0 "MH8" V 6654 4149 50  0000 L CNN
F 1 "+50V" V 6745 4149 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 6700 4000 50  0001 C CNN
F 3 "~" H 6700 4000 50  0001 C CNN
	1    6700 4000
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH7
U 1 1 5B88C7FE
P 6600 2950
F 0 "MH7" V 6554 3100 50  0000 L CNN
F 1 "GND" V 6645 3100 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 6600 2950 50  0001 C CNN
F 3 "~" H 6600 2950 50  0001 C CNN
	1    6600 2950
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH6
U 1 1 5B88C88A
P 6700 3300
F 0 "MH6" V 6654 3449 50  0000 L CNN
F 1 "+50V" V 6745 3449 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 6700 3300 50  0001 C CNN
F 3 "~" H 6700 3300 50  0001 C CNN
	1    6700 3300
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R5
U 1 1 5B8906CF
P 4400 3150
F 0 "R5" V 4193 3150 50  0000 C CNN
F 1 "330" V 4284 3150 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4330 3150 50  0001 C CNN
F 3 "~" H 4400 3150 50  0001 C CNN
F 4 "https://www.mouser.com/ProductDetail/Yageo/RC0402FR-07330RL?qs=sGAEpiMZZMtlubZbdhIBIOcXbDHk16D4kiv%2fASbsIsc%3d" V 4400 3150 50  0001 C CNN "ProductLink"
	1    4400 3150
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 5B8907B9
P 4400 3850
F 0 "R6" V 4193 3850 50  0000 C CNN
F 1 "330" V 4284 3850 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 4330 3850 50  0001 C CNN
F 3 "~" H 4400 3850 50  0001 C CNN
F 4 "https://www.mouser.com/ProductDetail/Yageo/RC0402FR-07330RL?qs=sGAEpiMZZMtlubZbdhIBIOcXbDHk16D4kiv%2fASbsIsc%3d" V 4400 3850 50  0001 C CNN "ProductLink"
	1    4400 3850
	0    1    1    0   
$EndComp
Wire Wire Line
	4550 3150 4700 3150
Wire Wire Line
	4700 3850 4550 3850
$Comp
L Mechanical:MountingHole_Pad MH4
U 1 1 5B891C8A
P 3750 3300
F 0 "MH4" V 3987 3305 50  0000 C CNN
F 1 "GND" V 3896 3305 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 3750 3300 50  0001 C CNN
F 3 "~" H 3750 3300 50  0001 C CNN
	1    3750 3300
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH3
U 1 1 5B891E2E
P 3750 2950
F 0 "MH3" V 3987 2955 50  0000 C CNN
F 1 "BCM_22" V 3896 2955 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 3750 2950 50  0001 C CNN
F 3 "~" H 3750 2950 50  0001 C CNN
	1    3750 2950
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH5
U 1 1 5B891F09
P 3750 3650
F 0 "MH5" V 3987 3655 50  0000 C CNN
F 1 "BCM_23" V 3896 3655 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 3750 3650 50  0001 C CNN
F 3 "~" H 3750 3650 50  0001 C CNN
	1    3750 3650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3850 3300 4250 3300
Wire Wire Line
	4250 3300 4250 3150
Wire Wire Line
	4250 3300 4250 3850
Connection ~ 4250 3300
Wire Wire Line
	4700 2950 3850 2950
Wire Wire Line
	4700 3650 3850 3650
Wire Wire Line
	5350 3050 5300 3050
Wire Wire Line
	5350 3050 5350 3750
Wire Wire Line
	5350 3750 5300 3750
$Comp
L Mechanical:MountingHole_Pad MH10
U 1 1 5B89B1F7
P 3750 4350
F 0 "MH10" V 3987 4355 50  0000 C CNN
F 1 "GND" V 3896 4355 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 3750 4350 50  0001 C CNN
F 3 "~" H 3750 4350 50  0001 C CNN
	1    3750 4350
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH11
U 1 1 5B89B2F5
P 3750 4650
F 0 "MH11" V 3796 4800 50  0000 L CNN
F 1 "MountingHole_Pad" V 3705 4800 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 3750 4650 50  0001 C CNN
F 3 "~" H 3750 4650 50  0001 C CNN
	1    3750 4650
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH12
U 1 1 5B89B48C
P 3750 4950
F 0 "MH12" V 3796 5100 50  0000 L CNN
F 1 "BCM_18" V 3705 5100 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 3750 4950 50  0001 C CNN
F 3 "~" H 3750 4950 50  0001 C CNN
	1    3750 4950
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH13
U 1 1 5B89B5FC
P 3750 5250
F 0 "MH13" V 3796 5400 50  0000 L CNN
F 1 "BCM_17" V 3705 5400 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 3750 5250 50  0001 C CNN
F 3 "~" H 3750 5250 50  0001 C CNN
	1    3750 5250
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH14
U 1 1 5B89B6E1
P 3750 5550
F 0 "MH14" V 3796 5700 50  0000 L CNN
F 1 "+3.3V" V 3705 5700 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 3750 5550 50  0001 C CNN
F 3 "~" H 3750 5550 50  0001 C CNN
	1    3750 5550
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH15
U 1 1 5B8A19FF
P 5700 4450
F 0 "MH15" V 5654 4600 50  0000 L CNN
F 1 "HangupSW" V 5750 4600 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 5700 4450 50  0001 C CNN
F 3 "~" H 5700 4450 50  0001 C CNN
	1    5700 4450
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH21
U 1 1 5B8A1B5E
P 5700 4850
F 0 "MH21" V 5650 5050 50  0000 L CNN
F 1 "Rotor" V 5750 5050 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 5700 4850 50  0001 C CNN
F 3 "~" H 5700 4850 50  0001 C CNN
	1    5700 4850
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH22
U 1 1 5B8A1C30
P 5700 5250
F 0 "MH22" V 5650 5450 50  0000 L CNN
F 1 "Dialing" V 5750 5450 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 5700 5250 50  0001 C CNN
F 3 "~" H 5700 5250 50  0001 C CNN
	1    5700 5250
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH20
U 1 1 5B8A6767
P 5800 5450
F 0 "MH20" V 5850 5250 50  0000 C CNN
F 1 "+3.3V" V 5750 5250 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 5800 5450 50  0001 C CNN
F 3 "~" H 5800 5450 50  0001 C CNN
	1    5800 5450
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH18
U 1 1 5B8A6937
P 5800 5050
F 0 "MH18" V 5850 4850 50  0000 C CNN
F 1 "+3.3V" V 5750 4850 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 5800 5050 50  0001 C CNN
F 3 "~" H 5800 5050 50  0001 C CNN
	1    5800 5050
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH16
U 1 1 5B8ABA18
P 5800 4650
F 0 "MH16" V 5850 4450 50  0000 C CNN
F 1 "+3.3V" V 5750 4450 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 5800 4650 50  0001 C CNN
F 3 "~" H 5800 4650 50  0001 C CNN
	1    5800 4650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5900 4650 5900 5050
Connection ~ 5900 5450
Wire Wire Line
	5900 5450 5900 5550
Wire Wire Line
	5900 5050 5900 5450
Connection ~ 5900 5050
$Comp
L Mouser:SN74LVC3G14 U5
U 1 1 5B88CCB3
P 4850 5150
F 0 "U5" H 4825 4435 50  0000 C CNN
F 1 "SN74LVC3G14" H 4825 4526 50  0000 C CNN
F 2 "Package_SO:VSSOP-8_2.4x2.1mm_P0.5mm" H 4850 5150 50  0001 C CNN
F 3 "" H 4850 5150 50  0001 C CNN
F 4 "https://www.mouser.com/ProductDetail/Texas-Instruments/SN74LVC3G14DCUR?qs=sGAEpiMZZMutVWjHE%2fYQw9XSWsf7N%2fuykB2S3rf7v6s%3d" H 4850 5150 50  0001 C CNN "ProductLink"
	1    4850 5150
	-1   0    0    1   
$EndComp
Wire Wire Line
	3850 5550 4300 5550
Wire Wire Line
	3850 4350 5150 4350
Wire Wire Line
	5150 4350 5150 4700
Wire Wire Line
	5600 4450 5350 4450
Wire Wire Line
	5350 4450 5350 4800
Wire Wire Line
	5350 4800 5150 4800
Wire Wire Line
	5600 4850 5300 4850
Wire Wire Line
	5300 4850 5300 5000
Wire Wire Line
	5300 5000 5150 5000
Wire Wire Line
	5600 5250 4450 5250
Wire Wire Line
	4450 5250 4450 4800
Wire Wire Line
	4450 4800 4600 4800
Wire Wire Line
	4600 4700 3850 4700
Wire Wire Line
	3850 4700 3850 4650
Wire Wire Line
	4600 4900 3900 4900
Wire Wire Line
	3900 4900 3900 4950
Wire Wire Line
	3900 4950 3850 4950
Wire Wire Line
	5150 4900 5400 4900
Wire Wire Line
	5400 4900 5400 5350
Wire Wire Line
	5400 5350 3850 5350
Wire Wire Line
	3850 5350 3850 5250
Wire Wire Line
	4600 5000 4300 5000
Wire Wire Line
	4300 5000 4300 5550
Connection ~ 4300 5550
Wire Wire Line
	4300 5550 5900 5550
$Comp
L Regulator_Switching:MC34063AD U4
U 1 1 5B88D302
P 2600 3200
F 0 "U4" H 2600 3667 50  0000 C CNN
F 1 "MC34063AD" H 2600 3576 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 2650 2750 50  0001 L CNN
F 3 "http://www.onsemi.com/pub_link/Collateral/MC34063A-D.PDF" H 3100 3100 50  0001 C CNN
	1    2600 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 3000 1750 3000
$Comp
L Device:C C4
U 1 1 5B895694
P 2250 3800
F 0 "C4" V 2502 3800 50  0000 C CNN
F 1 "1000pF" V 2411 3800 50  0000 C CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 2288 3650 50  0001 C CNN
F 3 "~" H 2250 3800 50  0001 C CNN
F 4 "https://www.mouser.com/ProductDetail/Murata-Electronics/GRM0335C1E102JA01D?qs=sGAEpiMZZMsh%252b1woXyUXj7CLgL0We8J5YTfhBz7QaJI%3d" V 2250 3800 50  0001 C CNN "ProductLink"
	1    2250 3800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2200 3400 2100 3400
Wire Wire Line
	2100 3400 2100 3800
Wire Wire Line
	2400 3800 2450 3800
Wire Wire Line
	2600 3800 2600 3700
$Comp
L Device:L L1
U 1 1 5B8A0B53
P 3150 3950
F 0 "L1" H 3203 3996 50  0000 L CNN
F 1 "560uH" H 3203 3905 50  0000 L CNN
F 2 "Inductor_SMD:L_Bourns-SRR1005" H 3150 3950 50  0001 C CNN
F 3 "~" H 3150 3950 50  0001 C CNN
F 4 "https://www.mouser.com/ProductDetail/Bourns/SRR1005-561K?qs=sGAEpiMZZMsg%252by3WlYCkU%252bKatOt54NSZYewpZaeKtDA%3d" H 3150 3950 50  0001 C CNN "ProductLink"
	1    3150 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 3400 3150 3400
Wire Wire Line
	3150 3400 3150 3800
Wire Wire Line
	2600 3700 3250 3700
Connection ~ 2600 3700
Wire Wire Line
	3000 3000 3100 3000
Wire Wire Line
	3100 3000 3100 3100
Wire Wire Line
	3100 3100 3100 3200
Connection ~ 3100 3100
Wire Wire Line
	3000 3100 3100 3100
Wire Wire Line
	3100 3200 3000 3200
Connection ~ 3100 3000
Wire Wire Line
	3100 2550 3100 3000
$Comp
L Device:R R7
U 1 1 5B8A19CA
P 3100 2400
F 0 "R7" H 3170 2446 50  0000 L CNN
F 1 "1" H 3170 2355 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3030 2400 50  0001 C CNN
F 3 "~" H 3100 2400 50  0001 C CNN
	1    3100 2400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 4100 3150 4250
$Comp
L Device:R R3
U 1 1 5B8F7193
P 3000 4250
F 0 "R3" V 2793 4250 50  0000 C CNN
F 1 "3k" V 2884 4250 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 2930 4250 50  0001 C CNN
F 3 "~" H 3000 4250 50  0001 C CNN
F 4 "https://www.mouser.com/ProductDetail/Panasonic/ERJ-U03J302V?qs=sGAEpiMZZMtlubZbdhIBIKJy1u7Ks2lIbflbml9piKA%3d" V 3000 4250 50  0001 C CNN "ProductLink"
	1    3000 4250
	0    1    1    0   
$EndComp
Connection ~ 3150 4250
$Comp
L Device:R R2
U 1 1 5B8F72AE
P 2600 3950
F 0 "R2" H 2670 3996 50  0000 L CNN
F 1 "1k" H 2670 3905 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2530 3950 50  0001 C CNN
F 3 "~" H 2600 3950 50  0001 C CNN
F 4 "https://www.mouser.com/ProductDetail/Yageo/RC0402FR-7W1KL?qs=sGAEpiMZZMtlubZbdhIBIE8A1EWi7VIOFmt6atdNJvs%3d" H 2600 3950 50  0001 C CNN "ProductLink"
	1    2600 3950
	1    0    0    -1  
$EndComp
Connection ~ 2600 3800
Wire Wire Line
	2600 4100 2600 4150
Wire Wire Line
	2600 4250 2850 4250
Wire Wire Line
	3000 3500 3000 4150
Wire Wire Line
	3000 4150 2600 4150
Connection ~ 2600 4150
Wire Wire Line
	2600 4150 2600 4250
Wire Wire Line
	2450 3800 2450 4000
Wire Wire Line
	2450 4000 1750 4000
Wire Wire Line
	1750 4000 1750 3400
Connection ~ 2450 3800
Wire Wire Line
	2450 3800 2600 3800
$Comp
L Device:CP1 C1
U 1 1 5B9104E3
P 1750 3250
F 0 "C1" H 1865 3296 50  0000 L CNN
F 1 "100uF" H 1865 3205 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x5.3" H 1750 3250 50  0001 C CNN
F 3 "~" H 1750 3250 50  0001 C CNN
F 4 "https://www.mouser.com/ProductDetail/Lelon/VE-101M1CTR-0605?qs=sGAEpiMZZMtZ1n0r9vR22edRdWtfW0D7MdzNEWEzyZU%3d" H 1750 3250 50  0001 C CNN "ProductLink"
	1    1750 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 3000 1750 3100
Wire Wire Line
	1750 2250 1750 3000
Connection ~ 1750 3000
$Comp
L Mechanical:MountingHole_Pad MH1
U 1 1 5B91E815
P 2150 4500
F 0 "MH1" V 2387 4505 50  0000 C CNN
F 1 "+5V" V 2296 4505 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 2150 4500 50  0001 C CNN
F 3 "~" H 2150 4500 50  0001 C CNN
	1    2150 4500
	0    -1   -1   0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH2
U 1 1 5B91E95F
P 2150 4900
F 0 "MH2" V 2387 4905 50  0000 C CNN
F 1 "GND" V 2296 4905 50  0000 C CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 2150 4900 50  0001 C CNN
F 3 "~" H 2150 4900 50  0001 C CNN
	1    2150 4900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3150 4500 2600 4500
Wire Wire Line
	3150 4250 3150 4500
Wire Wire Line
	2450 4000 2450 4900
Wire Wire Line
	2450 4900 2250 4900
Connection ~ 2450 4000
$Comp
L Device:D_Schottky D1
U 1 1 5B928A4A
P 2750 3800
F 0 "D1" H 2750 4016 50  0000 C CNN
F 1 "D_Schottky" H 2750 3925 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-523" H 2750 3800 50  0001 C CNN
F 3 "~" H 2750 3800 50  0001 C CNN
F 4 "https://www.mouser.com/ProductDetail/ON-Semiconductor-Fairchild/BAT43XV2?qs=sGAEpiMZZMtQ8nqTKtFS%2fImrW838HvlQCW36BTw3ZGU%3d" H 2750 3800 50  0001 C CNN "ProductLink"
	1    2750 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3800 2900 3800
Connection ~ 3150 3800
Wire Wire Line
	1750 2250 3100 2250
$Comp
L Regulator_Switching:MC34063AD U1
U 1 1 5B93CC91
P 2550 1300
F 0 "U1" H 2550 1767 50  0000 C CNN
F 1 "MC34063AD" H 2550 1676 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 2600 850 50  0001 L CNN
F 3 "http://www.onsemi.com/pub_link/Collateral/MC34063A-D.PDF" H 3050 1200 50  0001 C CNN
	1    2550 1300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH17
U 1 1 5B93CEDE
P 3450 800
F 0 "MH17" H 3350 758 50  0000 R CNN
F 1 "+12V" H 3350 849 50  0000 R CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 3450 800 50  0001 C CNN
F 3 "~" H 3450 800 50  0001 C CNN
	1    3450 800 
	-1   0    0    1   
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH19
U 1 1 5B93CF8F
P 3750 800
F 0 "MH19" H 3650 758 50  0000 R CNN
F 1 "GND" H 3650 849 50  0000 R CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 3750 800 50  0001 C CNN
F 3 "~" H 3750 800 50  0001 C CNN
	1    3750 800 
	-1   0    0    1   
$EndComp
Wire Wire Line
	3450 700  3300 700 
Wire Wire Line
	1750 700  1750 1100
Connection ~ 1750 2250
Wire Wire Line
	3750 700  3550 700 
Wire Wire Line
	2550 1800 3150 1800
Connection ~ 3250 1800
Wire Wire Line
	3250 1800 3350 1800
$Comp
L Device:C C3
U 1 1 5B960C10
P 2150 1900
F 0 "C3" V 2402 1900 50  0000 C CNN
F 1 "1000pF" V 2311 1900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 2188 1750 50  0001 C CNN
F 3 "~" H 2150 1900 50  0001 C CNN
F 4 "https://www.mouser.com/ProductDetail/Murata-Electronics/GRM0335C1E102JA01D?qs=sGAEpiMZZMsh%252b1woXyUXj7CLgL0We8J5YTfhBz7QaJI%3d" V 2150 1900 50  0001 C CNN "ProductLink"
	1    2150 1900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2550 1900 2300 1900
Connection ~ 2550 1800
Wire Wire Line
	2150 1500 2000 1500
Wire Wire Line
	2000 1500 2000 1900
Wire Wire Line
	2150 1100 1900 1100
Connection ~ 1750 1100
$Comp
L Device:CP1 C2
U 1 1 5B969F1F
P 1900 1250
F 0 "C2" H 2015 1296 50  0000 L CNN
F 1 "100uF" H 2015 1205 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x5.3" H 1900 1250 50  0001 C CNN
F 3 "~" H 1900 1250 50  0001 C CNN
F 4 "https://www.mouser.com/ProductDetail/Lelon/VE-101M1CTR-0605?qs=sGAEpiMZZMtZ1n0r9vR22edRdWtfW0D7MdzNEWEzyZU%3d" H 1900 1250 50  0001 C CNN "ProductLink"
	1    1900 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 1100 1750 2250
Connection ~ 1900 1100
Wire Wire Line
	1900 1100 1750 1100
Wire Wire Line
	1900 1400 1900 2000
Wire Wire Line
	1900 2000 2550 2000
Wire Wire Line
	2550 1800 2550 1900
Connection ~ 2550 1900
Wire Wire Line
	2550 1900 2550 2000
$Comp
L Device:R R1
U 1 1 5B974039
P 2050 900
F 0 "R1" V 1843 900 50  0000 C CNN
F 1 "0.25" V 1934 900 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 1980 900 50  0001 C CNN
F 3 "~" H 2050 900 50  0001 C CNN
F 4 "https://www.mouser.com/ProductDetail/Yageo/PT1206FR-070R25L?qs=sGAEpiMZZMtlubZbdhIBIM1WrCyzuseDbRB2u5Ld5wA%3d" V 2050 900 50  0001 C CNN "ProductLink"
	1    2050 900 
	0    1    1    0   
$EndComp
Wire Wire Line
	1900 900  1900 1100
$Comp
L Device:R R4
U 1 1 5B977826
P 3100 1200
F 0 "R4" V 2893 1200 50  0000 C CNN
F 1 "180" V 2984 1200 50  0000 C CNN
F 2 "Resistor_SMD:R_1206_3216Metric" V 3030 1200 50  0001 C CNN
F 3 "~" H 3100 1200 50  0001 C CNN
F 4 "https://www.mouser.com/ProductDetail/Panasonic-Industrial-Devices/ERJ-8ENF1800V?qs=sGAEpiMZZMtlubZbdhIBIOIpaOjkYUZGw6asExmtGIY%3d" V 3100 1200 50  0001 C CNN "ProductLink"
	1    3100 1200
	0    1    1    0   
$EndComp
Wire Wire Line
	2950 1100 3250 1100
Wire Wire Line
	3250 1100 3250 1200
Wire Wire Line
	3250 1100 3250 900 
Wire Wire Line
	3250 900  2200 900 
Connection ~ 3250 1100
Wire Wire Line
	2950 1500 3150 1500
Wire Wire Line
	3150 1500 3150 1800
Connection ~ 3150 1800
Wire Wire Line
	3150 1800 3250 1800
Wire Wire Line
	3250 1800 3250 3700
$Comp
L Device:L L2
U 1 1 5B9CFA53
P 3750 1200
F 0 "L2" V 3940 1200 50  0000 C CNN
F 1 "150uH" V 3849 1200 50  0000 C CNN
F 2 "Inductor_SMD:L_Bourns-SRN4018" H 3750 1200 50  0001 C CNN
F 3 "~" H 3750 1200 50  0001 C CNN
F 4 "https://www.mouser.com/ProductDetail/Bourns/SRN4018TA-151M?qs=sGAEpiMZZMsg%252by3WlYCkU3WTF7CL7b54EyQS9cCsn7E%3d" V 3750 1200 50  0001 C CNN "ProductLink"
	1    3750 1200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3250 1200 3600 1200
Connection ~ 3250 1200
Wire Wire Line
	3900 1200 3900 1300
Wire Wire Line
	2950 1300 3900 1300
$Comp
L Device:D_Schottky D2
U 1 1 5B9E3B70
P 3900 1450
F 0 "D2" V 3946 1371 50  0000 R CNN
F 1 "D_Schottky" V 3855 1371 50  0000 R CNN
F 2 "Diode_SMD:D_SOD-523" H 3900 1450 50  0001 C CNN
F 3 "~" H 3900 1450 50  0001 C CNN
F 4 "https://www.mouser.com/ProductDetail/ON-Semiconductor-Fairchild/BAT43XV2?qs=sGAEpiMZZMtQ8nqTKtFS%2fImrW838HvlQCW36BTw3ZGU%3d" V 3900 1450 50  0001 C CNN "ProductLink"
	1    3900 1450
	0    -1   -1   0   
$EndComp
Connection ~ 3900 1300
$Comp
L Device:R R9
U 1 1 5B9EBE57
P 3750 2250
F 0 "R9" V 3543 2250 50  0000 C CNN
F 1 "47k" V 3634 2250 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 3680 2250 50  0001 C CNN
F 3 "~" H 3750 2250 50  0001 C CNN
F 4 "https://www.mouser.com/ProductDetail/Yageo/RC0603JR-0747KL?qs=sGAEpiMZZMtlubZbdhIBILfEtiQlSflFUg9CwkcRAIY%3d" V 3750 2250 50  0001 C CNN "ProductLink"
	1    3750 2250
	0    1    1    0   
$EndComp
Connection ~ 3900 2250
$Comp
L Device:R R8
U 1 1 5B9EBFB5
P 3350 1950
F 0 "R8" H 3420 1996 50  0000 L CNN
F 1 "1k" H 3420 1905 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3280 1950 50  0001 C CNN
F 3 "~" H 3350 1950 50  0001 C CNN
F 4 "https://www.mouser.com/ProductDetail/Yageo/RC0402FR-7W1KL?qs=sGAEpiMZZMtlubZbdhIBIE8A1EWi7VIOFmt6atdNJvs%3d" H 3350 1950 50  0001 C CNN "ProductLink"
	1    3350 1950
	1    0    0    -1  
$EndComp
Connection ~ 3350 1800
Wire Wire Line
	3350 1800 3550 1800
Wire Wire Line
	3350 2100 3350 2250
Wire Wire Line
	3350 2250 3450 2250
Wire Wire Line
	2950 1600 3450 1600
Wire Wire Line
	3450 1600 3450 2250
Connection ~ 3450 2250
Wire Wire Line
	3450 2250 3600 2250
Wire Wire Line
	5500 700  3750 700 
Connection ~ 3750 700 
Wire Wire Line
	6500 3650 6250 3650
Wire Wire Line
	6250 3650 6250 3550
Wire Wire Line
	6250 3550 5900 3550
Wire Wire Line
	6500 2950 6300 2950
Wire Wire Line
	6300 2950 6300 2850
Wire Wire Line
	6300 2850 5900 2850
Wire Wire Line
	5900 3250 5500 3250
Wire Wire Line
	5500 3250 5500 700 
Wire Wire Line
	5900 3950 5500 3950
Wire Wire Line
	5500 3950 5500 3250
Connection ~ 5500 3250
Wire Wire Line
	6800 2250 6800 3300
Connection ~ 6800 3300
Wire Wire Line
	6800 3300 6800 4000
$Comp
L Device:CP1 C6
U 1 1 5B8A3348
P 4050 2100
F 0 "C6" H 3935 2054 50  0000 R CNN
F 1 "1000uF" H 3935 2145 50  0000 R CNN
F 2 "Capacitor_SMD:CP_Elec_18x22" H 4050 2100 50  0001 C CNN
F 3 "~" H 4050 2100 50  0001 C CNN
F 4 "https://www.mouser.com/ProductDetail/Lelon/VE-101M1CTR-0605?qs=sGAEpiMZZMtZ1n0r9vR22edRdWtfW0D7MdzNEWEzyZU%3d" V 4050 2100 50  0001 C CNN "ProductLink"
	1    4050 2100
	-1   0    0    1   
$EndComp
Wire Wire Line
	3550 700  3550 1750
Connection ~ 3550 1750
Wire Wire Line
	3550 1750 3550 1800
$Comp
L Device:CP1 C5
U 1 1 5B8B5B53
P 2600 4750
F 0 "C5" H 2715 4796 50  0000 L CNN
F 1 "100uF" H 2715 4705 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_6.3x5.3" H 2600 4750 50  0001 C CNN
F 3 "~" H 2600 4750 50  0001 C CNN
F 4 "https://www.mouser.com/ProductDetail/Lelon/VE-101M1CTR-0605?qs=sGAEpiMZZMtZ1n0r9vR22edRdWtfW0D7MdzNEWEzyZU%3d" H 2600 4750 50  0001 C CNN "ProductLink"
	1    2600 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 4500 2600 4600
Connection ~ 2600 4500
Wire Wire Line
	2600 4500 2250 4500
Wire Wire Line
	2600 4900 2450 4900
Connection ~ 2450 4900
Wire Wire Line
	3900 1600 3900 2250
Connection ~ 4050 2250
Wire Wire Line
	4050 2250 6800 2250
Wire Wire Line
	3900 2250 4050 2250
Wire Wire Line
	4050 1750 4050 1950
Wire Wire Line
	3550 1750 4050 1750
Wire Wire Line
	5350 3050 5350 950 
Wire Wire Line
	5350 950  3300 950 
Wire Wire Line
	3300 950  3300 700 
Connection ~ 5350 3050
Connection ~ 3300 700 
Wire Wire Line
	3300 700  1750 700 
$Comp
L Connector:AudioJack4_Ground J1
U 1 1 5B8A4C25
P 4450 1300
F 0 "J1" H 4416 1642 50  0000 C CNN
F 1 "AudioJack4_Ground" H 4416 1551 50  0000 C CNN
F 2 "Connector_Audio:Jack_3.5mm_PJ320D_Horizontal" H 4450 1300 50  0001 C CNN
F 3 "~" H 4450 1300 50  0001 C CNN
F 4 "https://www.amazon.com/uxcell-100Pcs-Female-Connector-PJ320D/dp/B00W8U8ZUC" H 4450 1300 50  0001 C CNN "ProductLink"
	1    4450 1300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH23
U 1 1 5B8A52BF
P 4900 1100
F 0 "MH23" V 4854 1250 50  0000 L CNN
F 1 "MountingHole_Pad" V 4945 1250 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 4900 1100 50  0001 C CNN
F 3 "~" H 4900 1100 50  0001 C CNN
	1    4900 1100
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH24
U 1 1 5B8A53B8
P 4900 1300
F 0 "MH24" V 4854 1450 50  0000 L CNN
F 1 "MountingHole_Pad" V 4945 1450 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 4900 1300 50  0001 C CNN
F 3 "~" H 4900 1300 50  0001 C CNN
	1    4900 1300
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH25
U 1 1 5B8A54F9
P 4900 1500
F 0 "MH25" V 4854 1650 50  0000 L CNN
F 1 "MountingHole_Pad" V 4945 1650 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 4900 1500 50  0001 C CNN
F 3 "~" H 4900 1500 50  0001 C CNN
	1    4900 1500
	0    1    1    0   
$EndComp
$Comp
L Mechanical:MountingHole_Pad MH26
U 1 1 5B8A55F8
P 4900 1700
F 0 "MH26" V 4854 1850 50  0000 L CNN
F 1 "MountingHole_Pad" V 4945 1850 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 4900 1700 50  0001 C CNN
F 3 "~" H 4900 1700 50  0001 C CNN
	1    4900 1700
	0    1    1    0   
$EndComp
Wire Wire Line
	4800 1100 4650 1100
Wire Wire Line
	4650 1100 4650 1200
Wire Wire Line
	4800 1300 4650 1300
Wire Wire Line
	4650 1400 4800 1400
Wire Wire Line
	4800 1400 4800 1500
Wire Wire Line
	4650 1500 4700 1500
Wire Wire Line
	4700 1500 4700 1700
Wire Wire Line
	4700 1700 4800 1700
$EndSCHEMATC
